library(shiny)
library(dplyr)
library(ggplot2)


patient = read.csv2("data/patient.csv")
location = read.csv2("data/location.csv")
hospital_stay = read.csv2("data/hospital_stay.csv")
hospital = read.csv2("data/hospital.csv")

complete_df = patient %>%
    merge(location, by = "location_id") %>%
    merge(hospital_stay, by = "patient_id") %>%
    merge(hospital, by = "hospital_id")

str(complete_df)

locations = unique(complete_df$location)
print(locations)
## Only run examples in interactive R sessions

    
ui <- fluidPage(
    checkboxGroupInput("selected_locations", "Patients from :",
                       c(locations)),
    tableOutput("data"),
    plotOutput("barplot")
)

server <- function(input, output, session) {
    
    output$data <- renderTable({
        print(input$variable2)
        
        complete_df %>%
            filter(location %in% input$selected_locations) %>%
            group_by(hospital) %>%
            summarize(number_stays = n())
        
    }, rownames = TRUE)
    
    output$barplot <- renderPlot({
        print(input$variable2)
        
        complete_df %>%
            filter(location %in% input$selected_locations) %>%
            group_by(hospital) %>%
            summarize(number_stays = n()) %>%
            ggplot(aes(x = hospital, y = number_stays)) +
            geom_col()
    })
}

shinyApp(ui, server)
    
